#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 9001

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["test-api.csproj" , "test/"]
RUN dotnet restore "test/test-api.csproj"
COPY . .
WORKDIR "/src/test"
RUN dotnet build "test-api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "test-api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "test-api.dll"]
